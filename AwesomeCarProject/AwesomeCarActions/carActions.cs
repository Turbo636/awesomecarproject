﻿using System;
using AwesomeCarProject;

namespace AwesomeCarActions
{
    public class carActions
    {
        public AwesomeCar car { get; set; }
        public carActions(string name, int mileage, int speed)
        {
            car = new AwesomeCar(name, mileage);

        }

        public bool CarSpeed_Accelerate(int speed)
        {
            if(speed > 0)
            {
                car.carSpeed += speed;
                Console.WriteLine($"Accelerating speed with {speed} km/h");
                Console.WriteLine($"{car.carName} is traveling at {car.carSpeed} km/h");
                return true;
            }
            return false;
        }

        public bool CarSpeed_decelerate(int speed)
        {
            
            if (speed > 0)
            {
                car.carSpeed -= speed;
                if(car.carSpeed > 0)
                {
                    Console.WriteLine($"Decelerate speed with {speed} km/h");
                    Console.WriteLine($"{car.carName} is traveling at {car.carSpeed} km/h");
                }
                
                if (car.carSpeed <= 0)
                {
                    Console.WriteLine($"Decelerate speed with {speed} km/h");
                    Console.WriteLine($"{car.carName} has come to a complete stop");
                }
                return true;
            }

            return false;
        }

        public bool Car_HeadligthsOn(bool headlights)
        {
            if(car.carHeadlightsOn == false)
            {
                car.carHeadlightsOn = true;
                Console.WriteLine("Turning on the headlights");
                return car.carHeadlightsOn;
            }else
            {
                car.carHeadlightsOn = false;
                Console.WriteLine("Turning off the headlights");
            }
            return car.carHeadlightsOn;
        }
    }
}

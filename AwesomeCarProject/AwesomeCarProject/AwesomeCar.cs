﻿using System;

namespace AwesomeCarProject
{
    /*
     * A car is defined as:
        1. It has a name
        2. It has mileage (you can use integers)
        3. It has a Current Speed
        4. It has a property for headlights that are on or off
        3.1 A car can accelerate (this will change current speed)
        3.2 A car can decelerate (this will also change current speed)
        3.4 A car can also turn it's headlights on or off

        Write me a LIBRARY, with unit tests covering this
     */
    public class AwesomeCar
    {
        public string carName { get; set; }
        public int carMileage { get; set; }
        public int carSpeed { get; set; }
        public bool carHeadlightsOn { get; set; }

        public AwesomeCar(string name, int mileage)
        {
            int speed = 0;
            carName = name;
            carMileage = mileage;
            carSpeed = speed;
            carHeadlightsOn = false;
        }
    }
}

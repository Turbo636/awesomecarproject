using Microsoft.VisualStudio.TestTools.UnitTesting;
using AwesomeCarProject;


namespace AwesomeCarProject.Test
{
    [TestClass]
    public class AwesomeCarProjectTest
    {
        [TestMethod]
        public void CarSpeed_AccelerateTest()
        {
            AwesomeCar car = new AwesomeCar("BMW", 10000);
            car.carSpeed += 50;
            Assert.IsTrue(car.carSpeed > 0);
        }

        [TestMethod]
        public void CarSpeed_decelerateTest()
        {
            AwesomeCar car = new AwesomeCar("BMW", 10000);
            car.carSpeed -= 50;
            Assert.IsFalse(car.carSpeed > 0);
        }

        [TestMethod]
        public void Car_HeadligthsOnTest()
        {
            AwesomeCar car = new AwesomeCar("BMW", 10000);
            car.carHeadlightsOn = true;
            Assert.IsTrue(car.carHeadlightsOn);
        }
    }
}
